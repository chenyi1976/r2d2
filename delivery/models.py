from django.contrib.auth.models import Group, User
from django.db import models


class PaymentInfo(models.Model):
    user = models.ForeignKey(User)
    info = models.CharField(max_length=200)

    def __unicode__(self):
        return 'PaymentInfo: {}'.format(self.user.username)


class PhoneType(models.Model):
    name = models.CharField(max_length=200)
    swap_default_price = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    buyback_default_price = models.DecimalField(max_digits=6, decimal_places=2, default=0)

    def __unicode__(self):
        return 'PhoneType: {}, {}, {}'.format(self.name, self.swap_default_price, self.buyback_default_price)

    def save(self, *args, **kwargs):
        super(PhoneType, self).save(*args, **kwargs)
        phone_variables = PhoneVariable.objects.filter(phone_type=self)
        if not phone_variables:
            phone_variable = PhoneVariable()
            phone_variable.phone_type = self
            phone_variable.name = 'Any Color'
            phone_variable.save()


class PhoneVariable(models.Model):
    phone_type = models.ForeignKey(PhoneType)
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return 'PhoneVariable: {}, {}'.format(self.name, self.phone_type.name)


# Total_Qualified, Pink_Mark, Non-OEM, Fixable_TP
class PhoneStatus(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return 'PhoneStatus: {}'.format(self.name)


class DeliveryStatus(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return 'DeliveryStatus: {}'.format(self.name)


# Swap, Buyback
class ProcessType(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return 'ProcessType: {}'.format(self.name)


class PhonePrice(models.Model):
    phone_type = models.ForeignKey(PhoneType)
    group = models.ForeignKey(Group)
    phone_status = models.ForeignKey(PhoneStatus)
    process_type = models.ForeignKey(ProcessType)
    price = models.DecimalField(max_digits=6, decimal_places=2, default=0)

    def __unicode__(self):
        return 'PhonePrice: {}, {}, {}, {}'.format(self.phone_type.name, self.group.name, self.phone_status.name,
                                                   self.price)


class Delivery(models.Model):
    user = models.ForeignKey(User)
    delivery_status = models.ForeignKey(DeliveryStatus)
    delivery_company = models.CharField(max_length=200)
    serial = models.CharField(max_length=200)
    start_date = models.DateField()
    receive_date = models.DateField(null=True)
    check_date = models.DateField(null=True)
    deleted = models.BooleanField(default=False)

    def __unicode__(self):
        return '[{}] Delivery({}): {}'.format(self.id, self.serial, self.user.username)


class DeliveryItem(models.Model):
    delivery = models.ForeignKey(Delivery)
    phone_type = models.ForeignKey(PhoneType)
    count = models.IntegerField(default=1)
    received = models.IntegerField(default=0)

    def __unicode__(self):
        return '[{}] Content of Delivery {}: {} {}'.format(self.id, self.delivery.serial, self.phone_type.name,
                                                           self.count)


class CheckResult(models.Model):
    delivery_item = models.ForeignKey(DeliveryItem)
    phone_status = models.ForeignKey(PhoneStatus)
    count = models.IntegerField(default=1)

    def __unicode__(self):
        return '[{}] CheckResult for DeliveryItem({}): {}, {}'.format(self.id, self.delivery_item.id,
                                                                      self.phone_status.name,
                                                                      self.count)


class Response(models.Model):
    check_result = models.ForeignKey(CheckResult)
    phone_variable = models.ForeignKey(PhoneVariable)
    process_count = models.IntegerField(default=1)
    response_date = models.DateTimeField()

    def __unicode__(self):
        return 'Response: ' + self.delivery.serial + ',' + self.phone_type.name + str(self.count)


class Setting(models.Model):
    setting_key = models.CharField(max_length=200)
    setting_value = models.CharField(max_length=200)

    def __unicode__(self):
        return 'Setting: ' + self.setting_key + ',' + self.setting_value
