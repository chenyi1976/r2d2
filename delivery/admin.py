from django.contrib import admin
from delivery.models import *

admin.site.register(PhoneType)
admin.site.register(PhoneStatus)
admin.site.register(PhonePrice)
# admin.site.register(ProcessType)
admin.site.register(Delivery)
admin.site.register(DeliveryItem)
admin.site.register(DeliveryStatus)
admin.site.register(PaymentInfo)
admin.site.register(PhoneVariable)
admin.site.register(Setting)
