"""r2d2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from delivery.views import *

urlpatterns = [
    # front page
    url(r'^$', 'delivery.views.delivery_list'),

    # Delivery URLs
    url(r'^delivery$', 'delivery.views.delivery_list'),
    url(r'^delivery/all$', 'delivery.views.delivery_list_all'),
    # url(r'^delivery/new$', DeliveryListView.as_view(), {'deleted': False}),
    url(r'^delivery/add_update$', 'delivery.views.delivery_add_update'),
    url(r'^delivery/delete', 'delivery.views.delivery_delete'),
    url(r'^delivery/detail/(?P<delivery_id>[\w-]+)', 'delivery.views.delivery_detail'),
    url(r'^delivery/detail_print/(?P<delivery_id>[\w-]+)', 'delivery.views.delivery_detail_print'),

    # url(r'^delivery/detail_taken', 'delivery.views.delivery_detail_taken'),
    url(r'^delivery/taken', 'delivery.views.delivery_taken'),
    url(r'^delivery/check', 'delivery.views.delivery_check'),
    url(r'^delivery/feedback', 'delivery.views.delivery_feedback'),

    #PhonePrice Summary
    url(r'^price_summary$', 'delivery.views.price'),

    #PhonePrice
    url(r'^price$', ListView.as_view(model=PhonePrice), name="price_list"),
    url(r'^price/detail/(?P<pk>[\w-]+)',
        UpdateView.as_view(model=PhonePrice, fields=['phone_type', 'group', 'phone_status', 'process_type', 'price'],
                           template_name_suffix="_detail", success_url='/price'), name="price_detail"),
    url(r'^price/add',
        CreateView.as_view(model=PhonePrice, fields=['phone_type', 'group', 'phone_status', 'process_type', 'price'],
                           template_name_suffix="_detail", success_url='/price'), name="price_add"),
    url(r'^price/delete/(?P<pk>[\w-]+)',
        DeleteView.as_view(model=PhonePrice, template_name_suffix="_confirm_delete", success_url='/price'),
        name="price_delete"),

    #PhoneType
    url(r'^phone$', ListView.as_view(model=PhoneType), name="phone_type_list"),
    url(r'^phone/detail/(?P<pk>[\w-]+)',
        UpdateView.as_view(model=PhoneType, fields=['name', 'swap_default_price', 'buyback_default_price'],
                           template_name_suffix="_detail", success_url='/phone'), name="phone_type_detail"),
    url(r'^phone/add',
        CreateView.as_view(model=PhoneType, fields=['name', 'swap_default_price', 'buyback_default_price'],
                           template_name_suffix="_detail", success_url='/phone'), name="phone_type_add"),

    #PaymentInfo
    url(r'^payment$', ListView.as_view(model=PaymentInfo), name="phone_type_list"),
    url(r'^payment/detail/(?P<pk>[\w-]+)',
        UpdateView.as_view(model=PaymentInfo, form_class=PaymentInfoForm, template_name_suffix="_detail",
                           success_url='/payment'), name="phone_type_detail"),
    url(r'^payment/add',
        CreateView.as_view(model=PaymentInfo, fields=['user', 'info'], template_name_suffix="_detail",
                           success_url='/payment'), name="phone_type_add"),
    url(r'^payment/delete/(?P<pk>[\w-]+)',
        DeleteView.as_view(model=PaymentInfo, template_name_suffix="_confirm_delete", success_url='/payment'),
        name="phone_type_delete"),

    #PhoneVariable
    url(r'^phone_var$', ListView.as_view(model=PhoneVariable), name="phone_variable_list"),
    url(r'^phone_var/detail/(?P<pk>[\w-]+)',
        UpdateView.as_view(model=PhoneVariable, fields=['phone_type', 'name'], template_name_suffix="_detail",
                           success_url='/phone_var'), name="phone_variable_detail"),
    url(r'^phone_var/add',
        CreateView.as_view(model=PhoneVariable, fields=['phone_type', 'name'], template_name_suffix="_detail",
                           success_url='/phone_var'), name="phone_variable_add"),
    url(r'^phone_var/delete/(?P<pk>[\w-]+)',
        DeleteView.as_view(model=PhoneVariable, template_name_suffix="_confirm_delete", success_url='/phone_var'),
        name="phone_variable_delete"),

    #DeliveryStatus
    url(r'^delivery_status$', ListView.as_view(model=DeliveryStatus), name="delivery_status_list"),
    url(r'^delivery_status/detail/(?P<pk>\d+)',
        UpdateView.as_view(model=DeliveryStatus, fields=['name'], template_name_suffix="_detail",
                           success_url='/delivery_status'),
        name="delivery_status_detail"),
    url(r'^delivery_status/add',
        CreateView.as_view(model=DeliveryStatus, fields=['name'], template_name_suffix="_detail",
                           success_url='/delivery_status'), name="delivery_status_add"),
    url(r'^delivery_status/delete/(?P<pk>[\w-]+)',
        DeleteView.as_view(model=DeliveryStatus, template_name_suffix="_confirm_delete",
                           success_url='/delivery_status'),
        name="delivery_status_delete"),

]
