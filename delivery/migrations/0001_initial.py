# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CheckResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='Delivery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('delivery_company', models.CharField(max_length=200)),
                ('serial', models.CharField(max_length=200)),
                ('start_date', models.DateField()),
                ('receive_date', models.DateField(null=True)),
                ('check_date', models.DateField(null=True)),
                ('deleted', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='DeliveryItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(default=1)),
                ('received', models.IntegerField(default=0)),
                ('delivery', models.ForeignKey(to='delivery.Delivery')),
            ],
        ),
        migrations.CreateModel(
            name='DeliveryStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='PaymentInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('info', models.CharField(max_length=200)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='PhonePrice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.DecimalField(default=0, max_digits=6, decimal_places=2)),
                ('group', models.ForeignKey(to='auth.Group')),
            ],
        ),
        migrations.CreateModel(
            name='PhoneStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='PhoneType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('swap_default_price', models.DecimalField(default=0, max_digits=6, decimal_places=2)),
                ('buyback_default_price', models.DecimalField(default=0, max_digits=6, decimal_places=2)),
            ],
        ),
        migrations.CreateModel(
            name='PhoneVariable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('phone_type', models.ForeignKey(to='delivery.PhoneType')),
            ],
        ),
        migrations.CreateModel(
            name='ProcessType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Response',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('process_count', models.IntegerField(default=1)),
                ('response_date', models.DateTimeField()),
                ('check_result', models.ForeignKey(to='delivery.CheckResult')),
                ('phone_variable', models.ForeignKey(to='delivery.PhoneVariable')),
            ],
        ),
        migrations.CreateModel(
            name='Setting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('setting_key', models.CharField(max_length=200)),
                ('setting_value', models.CharField(max_length=200)),
            ],
        ),
        migrations.AddField(
            model_name='phoneprice',
            name='phone_status',
            field=models.ForeignKey(to='delivery.PhoneStatus'),
        ),
        migrations.AddField(
            model_name='phoneprice',
            name='phone_type',
            field=models.ForeignKey(to='delivery.PhoneType'),
        ),
        migrations.AddField(
            model_name='phoneprice',
            name='process_type',
            field=models.ForeignKey(to='delivery.ProcessType'),
        ),
        migrations.AddField(
            model_name='deliveryitem',
            name='phone_type',
            field=models.ForeignKey(to='delivery.PhoneType'),
        ),
        migrations.AddField(
            model_name='delivery',
            name='delivery_status',
            field=models.ForeignKey(to='delivery.DeliveryStatus'),
        ),
        migrations.AddField(
            model_name='delivery',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='checkresult',
            name='delivery_item',
            field=models.ForeignKey(to='delivery.DeliveryItem'),
        ),
        migrations.AddField(
            model_name='checkresult',
            name='phone_status',
            field=models.ForeignKey(to='delivery.PhoneStatus'),
        ),
    ]
