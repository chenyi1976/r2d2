import datetime
from django import forms
from delivery.models import *


class PaymentInfoForm(forms.ModelForm):
    info = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = PaymentInfo
        fields = '__all__'


class DeliveryForm(forms.Form):
    delivery_id = forms.IntegerField(required=False, widget=forms.HiddenInput(), initial='-1')
    delivery_status = forms.ModelChoiceField(label="Status", queryset=DeliveryStatus.objects.all())
    delivery_company = forms.CharField(label="Shipping Company", max_length=200)
    serial = forms.CharField(label="Serial", max_length=200)
    start_date = forms.DateField(label="Start Date", widget=forms.DateInput, help_text='yyyy-mm-dd',
                                 initial=datetime.datetime.now())

    def __init__(self, *args, **kwargs):

        user = None
        try:
            user = kwargs.pop('user')
        except KeyError:
            pass

        delivery_item_dict = None
        try:
            delivery_item_dict = kwargs.pop('delivery_item_dict')
        except KeyError:
            pass

        super(DeliveryForm, self).__init__(*args, **kwargs)

        if user and user.is_superuser:
            self.fields["user"] = forms.ModelChoiceField(label="User", queryset=User.objects.all())

        phone_types = PhoneType.objects.all()

        for phone_type in phone_types:
            count = 0
            if delivery_item_dict:
                if phone_type.id in delivery_item_dict:
                    count = delivery_item_dict[phone_type.id].count
            self.fields["send_phone_type_%d" % phone_type.id] = forms.IntegerField(label=phone_type.name, initial=count,
                                                                                   min_value=0)


class TakenForm(forms.Form):
    delivery_id = forms.IntegerField(required=False, widget=forms.HiddenInput(), initial=-1)

    def __init__(self, *args, **kwargs):

        delivery_item_dict = None
        try:
            delivery_item_dict = kwargs.pop('delivery_item_dict')
        except KeyError:
            pass

        super(TakenForm, self).__init__(*args, **kwargs)

        phone_types = PhoneType.objects.all()

        for phone_type in phone_types:
            count = 0
            if delivery_item_dict:
                if phone_type.id in delivery_item_dict:
                    count = delivery_item_dict[phone_type.id].received
            else:
                if args[0]:
                    try:
                        count = args[0]["taken_phone_type_%d" % phone_type.id]
                    except KeyError:
                        pass
            self.fields["taken_phone_type_%d" % phone_type.id] = forms.IntegerField(label=phone_type.name,
                                                                                    initial=count)


class CheckForm(forms.Form):
    delivery_id = forms.IntegerField(required=False, widget=forms.HiddenInput(), initial=-1)

    def __init__(self, *args, **kwargs):

        check_item_dict = None
        try:
            check_item_dict = kwargs.pop('check_item_dict')
        except KeyError:
            pass

        super(CheckForm, self).__init__(*args, **kwargs)

        phone_types = PhoneType.objects.all()
        phone_statuses = PhoneStatus.objects.all()

        for phone_type in phone_types:
            for phone_status in phone_statuses:
                count = 0
                if check_item_dict:
                    try:
                        count = check_item_dict[phone_type.id][phone_status.id]
                    except KeyError:
                        pass
                else:
                    if args:
                        try:
                            count = args[0]["check_%d_%d" % (phone_type.id, phone_status.id)]
                        except KeyError:
                            pass
                self.fields["check_%d_%d" % (phone_type.id, phone_status.id)] \
                    = forms.IntegerField(label="%s %s" % (phone_type.name, phone_status.name), initial=count)

    def clean(self):
        cleaned_data = super(CheckForm, self).clean()

        phone_types = PhoneType.objects.all()
        phone_statuses = PhoneStatus.objects.all()

        delivery = Delivery.objects.get(pk=cleaned_data.get("delivery_id"))
        delivery_items = DeliveryItem.objects.filter(delivery=delivery)

        for phone_type in phone_types:
            count = 0
            for phone_status in phone_statuses:
                status_count = cleaned_data.get("check_%d_%d" % (phone_type.id, phone_status.id))
                count += status_count
            taken_count = 0
            for delivery_item in delivery_items:
                if delivery_item.phone_type.id == phone_type.id:
                    taken_count = delivery_item.received
            if taken_count != count:
                raise forms.ValidationError(
                    "%s test result: '%d' not equals to receive count: '%d'" % (phone_type.name, count, taken_count)
                )


class FeedbackForm(forms.Form):
    delivery_id = forms.IntegerField(required=False, widget=forms.HiddenInput(), initial=-1)
    is_readonly = forms.BooleanField(required=False, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):

        feedback_item_dict = None
        try:
            feedback_item_dict = kwargs.pop('feedback_item_dict')
        except KeyError:
            pass

        super(FeedbackForm, self).__init__(*args, **kwargs)

        phone_types = PhoneType.objects.all()
        phone_statuses = PhoneStatus.objects.all()
        # process_types = ProcessType.objects.all()

        for phone_type in phone_types:
            for phone_status in phone_statuses:
                phone_variables = PhoneVariable.objects.filter(phone_type=phone_type)
                for phone_variable in phone_variables:
                    count = 0
                    if feedback_item_dict:
                        if (phone_type.id, phone_status.id) in feedback_item_dict:
                            response_dict = feedback_item_dict[(phone_type.id, phone_status.id)]
                            if phone_variable.id in response_dict:
                                count = response_dict[phone_variable.id]
                    else:
                        if args:
                            try:
                                count = args[0]["feedback_%d_%d_%d" % (phone_type.id, phone_status.id, phone_variable.id)]
                            except KeyError:
                                pass
                    self.fields["feedback_%d_%d_%d" % (phone_type.id, phone_status.id, phone_variable.id)] \
                        = forms.IntegerField(label="%s %s %s" % (phone_type.name, phone_status.name, phone_variable.name),
                                             initial=count)

    def clean(self):
        cleaned_data = super(FeedbackForm, self).clean()

        phone_types = PhoneType.objects.all()
        phone_statuses = PhoneStatus.objects.all()
        # process_types = ProcessType.objects.all()

        if cleaned_data.get("is_readonly"):
            raise forms.ValidationError(
                "You are not allowed to change, please contact super user for change"
            )

        delivery = Delivery.objects.get(pk=cleaned_data.get("delivery_id"))
        delivery_items = DeliveryItem.objects.filter(delivery=delivery)

        process_count_total = 0
        for phone_type in phone_types:
            for phone_status in phone_statuses:
                check_count = 0
                for delivery_item in delivery_items:
                    if delivery_item.phone_type.id == phone_type.id:
                        check_results = CheckResult.objects.filter(delivery_item=delivery_item)
                        for check_result in check_results:
                            if check_result.phone_status == phone_status:
                                check_count = check_result.count

                process_count = 0
                phone_variables = PhoneVariable.objects.filter(phone_type=phone_type)

                for phone_variable in phone_variables:
                    process_count += cleaned_data.get("feedback_%d_%d_%d" % (phone_type.id, phone_status.id, phone_variable.id))
                process_count_total += process_count
                if process_count > check_count:
                    raise forms.ValidationError(
                        "too many feedback result for %s %s, max is %d, now is %d " % (phone_type.name, phone_status.name, check_count, process_count)
                    )

        if process_count_total == 0:
            raise forms.ValidationError(
                "total process count is 0, can not save it."
            )
