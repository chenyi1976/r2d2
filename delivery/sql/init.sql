
delete from  delivery_phonestatus;
delete from  delivery_processtype;
delete from  delivery_phonevariable;
delete from  delivery_phonetype;
delete from  auth_user_groups;
delete from  auth_group;
delete from auth_user where `username` != 'admin';


INSERT INTO `auth_group`(`name`) VALUES ('g1');
INSERT INTO `auth_group`(`name`) VALUES ('g2');

insert into `auth_user`(`username`, `password`, `first_name`, `last_name`, `email`, `is_superuser`, `is_staff`, `is_active`, `date_joined`) VALUES ('c1', 'pbkdf2_sha256$20000$pDsmpv7JFDD1$v4jJzVN3CJiFozGvku8/6za4vRU7kFZz0gvFxhJH+f8=', 'first', 'last', 'test@test.com', 0, 0, 1, '2015-06-12 01:08:19.400099');
insert into `auth_user`(`username`, `password`, `first_name`, `last_name`, `email`, `is_superuser`, `is_staff`, `is_active`, `date_joined`) VALUES ('c2', 'pbkdf2_sha256$20000$pDsmpv7JFDD1$v4jJzVN3CJiFozGvku8/6za4vRU7kFZz0gvFxhJH+f8=', 'first', 'last', 'test@test.com', 0, 0, 1, '2015-06-12 01:08:19.400099');

insert into `auth_user_groups`(`user_id`, `group_id`) values ((select id from `auth_user` where `username`='c1'), (select id from `auth_group` where `name`='g1'));
insert into `auth_user_groups`(`user_id`, `group_id`) values ((select id from `auth_user` where `username`='c2'), (select id from `auth_group` where `name`='g2'));

INSERT INTO `delivery_phonetype`(`name`, `swap_default_price`, `buyback_default_price`) VALUES ('iphone4', 15, 15);
INSERT INTO `delivery_phonevariable`(`phone_type_id`, `name`) VALUES ((select id from `delivery_phonetype` where `name`='iphone4'), 'white');
INSERT INTO `delivery_phonevariable`(`phone_type_id`, `name`) VALUES ((select id from `delivery_phonetype` where `name`='iphone4'), 'black');


INSERT INTO `delivery_phonetype`(`name`, `swap_default_price`, `buyback_default_price`) VALUES ('iphone5', 20, 20);
INSERT INTO `delivery_phonevariable`(`phone_type_id`, `name`) VALUES ((select id from `delivery_phonetype` where `name`='iphone5'), 'white');
INSERT INTO `delivery_phonevariable`(`phone_type_id`, `name`) VALUES ((select id from `delivery_phonetype` where `name`='iphone5'), 'black');

INSERT INTO `delivery_processtype`(`name`) VALUES ('swap');
INSERT INTO `delivery_processtype`(`name`) VALUES ('buyback');

INSERT INTO `delivery_phonestatus`(`name`) VALUES ('Total_Qualified');
INSERT INTO `delivery_phonestatus`(`name`) VALUES ('Pink_Mark');
INSERT INTO `delivery_phonestatus`(`name`) VALUES ('Non-OEM');
INSERT INTO `delivery_phonestatus`(`name`) VALUES ('Fixable_TP');
INSERT INTO `delivery_phonestatus`(`name`) VALUES ('Bad');

INSERT INTO `delivery_deliverystatus`(`name`) VALUES ('Arrived in HK');
INSERT INTO `delivery_deliverystatus`(`name`) VALUES ('In Transit to SZ');
INSERT INTO `delivery_deliverystatus`(`name`) VALUES ('Arrived in SZ');
INSERT INTO `delivery_deliverystatus`(`name`) VALUES ('Testing');
INSERT INTO `delivery_deliverystatus`(`name`) VALUES ('Tested');
INSERT INTO `delivery_deliverystatus`(`name`) VALUES ('Confirmed');

INSERT INTO `delivery_phoneprice`(`group_id`, `phone_status_id`, `phone_type_id`, `process_type_id`, `price`)
  VALUES ((select id from `auth_group` where `name`='g1'),
          (select id from `delivery_phonestatus` where `name`='Total_Qualified'),
          (select id from `delivery_processtype` where `name`='swap'),
          (select id from `delivery_phonetype` where `name`='iphone4'),
          30
);

INSERT INTO `delivery_phoneprice`(`group_id`, `phone_status_id`, `phone_type_id`, `process_type_id`, `price`)
  VALUES ((select id from `auth_group` where `name`='g2'),
          (select id from `delivery_phonestatus` where `name`='Pink_Mark'),
          (select id from `delivery_processtype` where `name`='buyback'),
          (select id from `delivery_phonetype` where `name`='iphone5'),
          40
);

INSERT INTO `delivery_setting`(`setting_key`, `setting_value`) VALUES ('feedback_auto_lock', '21600');

