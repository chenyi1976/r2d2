from django.utils import timezone
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render
from django.views.generic import ListView

from delivery.forms import *
from delivery.models import *


def index(request):
    return render(request, 'delivery/index.html', {})


def price(request):
    table_data = []

    header_data = ['phone type', 'price type', 'phone_status']

    context = {}

    phone_types = PhoneType.objects.all()
    process_types = ProcessType.objects.all()
    phone_statuses = PhoneStatus.objects.all()
    groups = Group.objects.all()

    current_user = request.user
    if current_user.is_superuser:

        for group in groups:
            header_data.append(group.name)

        for phone_type in phone_types:
            for process_type in process_types:
                for phone_status in phone_statuses:
                    row_data = [phone_type.name, process_type.name, phone_status.name]
                    for group in groups:
                        phone_price = PhonePrice.objects.filter(phone_type=phone_type.id).filter(
                            process_type=process_type.id).filter(phone_status=phone_status.id).filter(group=group.id);
                        if phone_price and len(phone_price) > 0:
                            row_data.append("*" + str(phone_price[0].price))
                        else:
                            if process_type.name == 'swap':
                                row_data.append(phone_type.swap_default_price)
                            else:
                                row_data.append(phone_type.buyback_default_price)

                    table_data.append(row_data)

        context['is_super_user'] = True
        context['phone_types'] = phone_types
        phone_prices = PhonePrice.objects.all()
        context['phone_prices'] = phone_prices
    else:
        header_data.append('Price')

        user_groups = current_user.groups.all()

        user_group = user_groups[0]
        for phone_type in phone_types:
            for process_type in process_types:
                for phone_status in phone_statuses:
                    row_data = [phone_type.name, process_type.name, phone_status.name]
                    phone_price = PhonePrice.objects.filter(phone_type=phone_type.id).filter(
                        process_type=process_type.id).filter(phone_status=phone_status.id).filter(group=user_group.id);
                    if phone_price and len(phone_price) > 0:
                        row_data.append("*" + str(phone_price[0].price))
                    else:
                        if process_type.name == 'swap':
                            row_data.append(phone_type.swap_default_price)
                        else:
                            row_data.append(phone_type.buyback_default_price)

                    table_data.append(row_data)

        if user_groups:
            for user_group in user_groups:
                phone_prices = PhonePrice.objects.filter(group=user_group.id)

                context['phone_prices'] = phone_prices
        else:
            print 'user do not have group defined.'

    context['table_data'] = table_data;
    context['header_data'] = header_data;

    return render(request, 'delivery/phoneprice_summary.html', context)


class DeliveryListView(ListView):
    def get_queryset(self):
        current_user = self.request.user
        if current_user.is_superuser:
            query_set = Delivery.objects.all()
        else:
            query_set = Delivery.objects.filter(user=current_user.id)

        deleted_filter = self.request.GET.get("deleted")
        print 'deleted_filter' + str(self.request.GET)
        if deleted_filter:
            query_set.filter(deleted=bool(deleted_filter))

        return query_set


def delivery_list(request):
    context = {}

    current_user = request.user
    if current_user.is_superuser:
        deliveries = Delivery.objects.filter(deleted=False)
    else:
        deliveries = Delivery.objects.filter(user=current_user.id).filter(deleted=False)

    context['object_list'] = deliveries

    return render(request, 'delivery/delivery_list.html', context)


def delivery_list_all(request):
    context = {}

    current_user = request.user
    if current_user.is_superuser:
        deliveries = Delivery.objects.all()
    else:
        deliveries = Delivery.objects.filter(user=current_user.id)

    context['object_list'] = deliveries

    return render(request, 'delivery/delivery_list.html', context)


def delivery_detail(request, delivery_id):
    message = request.GET.get('message', None)
    return delivery_detail_with_message(request, delivery_id, message)


def delivery_detail_print(request, delivery_id):
    message = request.GET.get('message', None)
    return delivery_detail_with_message(request, delivery_id, message, is_print=True)


def delivery_detail_with_message(request, delivery_id, message=None, delivery_form=None, taken_form=None,
                                 check_form=None, feedback_form=None, is_print=False):
    delivery = Delivery.objects.get(pk=delivery_id)
    delivery_items = delivery.deliveryitem_set.iterator()

    delivery_item_dict = {}
    check_item_dict = {}
    feedback_item_dict = {}
    delta = datetime.timedelta(0, 0, 0)
    for delivery_item in delivery_items:
        delivery_item_dict[delivery_item.phone_type.id] = delivery_item
        check_results = CheckResult.objects.filter(delivery_item=delivery_item)
        for check_result in check_results:
            type_dict = {}
            if delivery_item.phone_type.id in check_item_dict:
                type_dict = check_item_dict[delivery_item.phone_type.id]
            else:
                check_item_dict[delivery_item.phone_type.id] = type_dict
            type_dict[check_result.phone_status.id] = check_result.count

            responses = Response.objects.filter(check_result=check_result)
            response_dict = {}
            feedback_item_dict[(delivery_item.phone_type.id, check_result.phone_status.id)] = response_dict
            for response in responses:
                response_dict[response.phone_variable.id] = response.process_count
                if response.response_date is not None:
                    delta = timezone.now() - response.response_date

    is_readonly = False
    if not request.user.is_superuser:
        if not is_readonly:
            if delta.total_seconds() > 0:
                lock_time = 0
                settings = Setting.objects.filter(setting_key='feedback_auto_lock')
                try:
                    lock_time = int(settings[0].setting_value)
                except (IndexError, ValueError):
                    pass
                if lock_time <= 0:
                    lock_time = 6*60*60
                is_readonly = delta > datetime.timedelta(0, lock_time, 0)

    if delivery_form is None:
        delivery_form = DeliveryForm(user=request.user,
                                     delivery_item_dict=delivery_item_dict,
                                     initial={'delivery_id': delivery.id,
                                              'user': delivery.user,
                                              'delivery_company': delivery.delivery_company,
                                              'serial': delivery.serial,
                                              'delivery_status': delivery.delivery_status,
                                              'start_date': delivery.start_date})

    if taken_form is None:
        taken_form = TakenForm(delivery_item_dict=delivery_item_dict,
                               initial={'delivery_id': delivery.id})

    if check_form is None:
        check_form = CheckForm(check_item_dict=check_item_dict,
                               initial={'delivery_id': delivery.id})

    if feedback_form is None:
        feedback_form = FeedbackForm(feedback_item_dict=feedback_item_dict,
                                     initial={'delivery_id': delivery.id, 'is_readonly': is_readonly})

    context = {'delivery_id': delivery.id,
               'message': message,
               'delivery_form': delivery_form,
               'taken_form': taken_form,
               'check_form': check_form,
               'feedback_form': feedback_form}
    if is_print:
        return render(request, 'delivery/delivery_detail_print.html', context)
    return render(request, 'delivery/delivery_detail.html', context)


def delivery_add_update(request):
    if request.method == 'GET':
        form = DeliveryForm(None, user=request.user)
        return render(request, "delivery/delivery_detail.html", {'delivery_form': form})

    form = DeliveryForm(request.POST, user=request.user)

    print request.POST
    print form.data

    if not form.is_valid():
        return render(request, "delivery/delivery_detail.html", {'delivery_form': form})

    delivery_id = form.cleaned_data['delivery_id']

    serial = form.cleaned_data['serial']
    delivery_company = form.cleaned_data['delivery_company']
    try:
        user = form.cleaned_data['user']
    except KeyError:
        user = request.user
    start_date = form.cleaned_data['start_date']
    delivery_status = form.cleaned_data['delivery_status']
    if delivery_id >= 0:
        delivery = Delivery.objects.get(pk=delivery_id)
        delivery.delivery_company = delivery_company
        delivery.serial = serial
        delivery.delivery_status = delivery_status
        delivery.start_date = start_date
        if user:
            delivery.user = user
        delivery.save()
    else:
        delivery = Delivery(user=user, serial=serial, delivery_company=delivery_company,
                            delivery_status=delivery_status, start_date=start_date)
        delivery.save()
        delivery_id = delivery.id

    for name, value in form.cleaned_data.items():
        if name.startswith('send_phone_type_'):
            phone_type_id = int(name[len('send_phone_type_'):])
            phone_type = PhoneType.objects.get(pk=phone_type_id)
            delivery_items = DeliveryItem.objects.filter(phone_type=phone_type, delivery=delivery)
            if delivery_items and len(delivery_items) > 0:
                delivery_item = delivery_items[0]
                delivery_item.count = value
                delivery_item.save()
            else:
                delivery_item = DeliveryItem(phone_type=phone_type, delivery=delivery, count=value)
                delivery_item.save()

    if delivery_id is None:
        if 'delivery_id' in form.cleaned_data:
            delivery_id = form.cleaned_data['delivery_id']

    if delivery_id >= 0:
        return delivery_detail_with_message(request, delivery_id, 'Order No: {} has been saved.'.format(delivery_id))
    else:
        return render(request, "delivery/delivery_detail.html", {'delivery_form': form})


def delivery_delete(request):
    if request.POST:
        try:
            delivery_id = request.POST['delivery_id']
            delivery = Delivery.objects.get(pk=delivery_id)

            if delivery.user != request.user:
                if not request.user.is_superuser:
                    return HttpResponseForbidden()

            delivery.deleted = not delivery.deleted
            delivery.save()

            return HttpResponseRedirect('/delivery')
        except KeyError:
            pass

    return HttpResponseRedirect('/delivery')


def delivery_taken(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden()

    form = TakenForm(request.POST or None)

    if form.is_valid():
        try:
            delivery_id = form.cleaned_data['delivery_id']
            delivery = Delivery.objects.get(pk=delivery_id)
            delivery.receive_date = timezone.now()
            delivery.save()

            for name, value in form.cleaned_data.items():
                if name.startswith('taken_phone_type_'):
                    phone_type_id = int(name[len('taken_phone_type_'):])
                    phone_type = PhoneType.objects.get(pk=phone_type_id)
                    delivery_items = DeliveryItem.objects.filter(phone_type=phone_type, delivery=delivery)
                    if delivery_items and len(delivery_items) > 0:
                        delivery_item = delivery_items[0]
                        delivery_item.received = value
                        delivery_item.save()

            return delivery_detail_with_message(request, delivery_id,
                                                'Received Detail for Order No: {} has been saved.'.format(delivery_id))
        except KeyError:
            pass

    # should return to origin page, and show the error message
    return delivery_detail_with_message(request, delivery_id,
                                        message='Failed to save Received Detail for Order No: {}.'.format(delivery_id),
                                        taken_form=form)
    # return HttpResponseRedirect('/delivery')


def delivery_check(request):
    if request.method == 'GET':
        return HttpResponseRedirect('/delivery')

    form = CheckForm(request.POST)
    delivery_id = request.POST['delivery_id']

    if form.is_valid():
        # try:
            delivery = Delivery.objects.get(pk=delivery_id)
            delivery.check_date = timezone.now()
            delivery.save()

            for name, value in form.cleaned_data.items():
                if name.startswith('check_'):
                    type_and_status = name[len('check_'):]
                    type_id, status_id = type_and_status.split('_')
                    phone_type = PhoneType.objects.get(pk=type_id)
                    phone_status = PhoneStatus.objects.get(pk=status_id)
                    delivery_items = DeliveryItem.objects.filter(delivery=delivery, phone_type=phone_type)
                    if delivery_items and len(delivery_items) > 0:
                        delivery_item = delivery_items[0]
                        check_results = CheckResult.objects.filter(delivery_item=delivery_item,
                                                                   phone_status=phone_status)
                        if check_results and len(check_results) > 0:
                            check_result = check_results[0]
                            check_result.count = value
                            check_result.save()
                        else:
                            check_result = CheckResult()
                            check_result.delivery_item = delivery_item
                            check_result.phone_status = phone_status
                            check_result.count = value
                            check_result.save()

            return delivery_detail_with_message(request, delivery_id,
                                                'Test Result for Order No: {} has been saved.'.format(delivery_id))
        # except KeyError:
        #     pass

    return delivery_detail_with_message(request, delivery_id,
                                        message='Failed to save Test Result for Order No: {}.'.format(delivery_id),
                                        check_form=form)


def delivery_feedback(request):
    if request.method == 'GET':
        return HttpResponseRedirect('/delivery')

    form = FeedbackForm(request.POST)
    delivery_id = request.POST['delivery_id']

    if form.is_valid():
        try:
            delivery = Delivery.objects.get(pk=delivery_id)
            delivery.check_date = timezone.now()
            delivery.save()

            for name, value in form.cleaned_data.items():
                if name.startswith('feedback_'):
                    type_and_status = name[len('feedback_'):]
                    type_id, status_id, phone_variable_id = type_and_status.split('_')
                    phone_type = PhoneType.objects.get(pk=type_id)
                    phone_status = PhoneStatus.objects.get(pk=status_id)
                    phone_variable = PhoneVariable.objects.get(pk=phone_variable_id)
                    delivery_items = DeliveryItem.objects.filter(delivery=delivery, phone_type=phone_type)
                    if delivery_items and len(delivery_items) > 0:
                        delivery_item = delivery_items[0]
                        check_results = CheckResult.objects.filter(delivery_item=delivery_item,
                                                                   phone_status=phone_status)
                        if check_results and len(check_results) > 0:
                            check_result = check_results[0]
                            responses = Response.objects.filter(check_result=check_result, phone_variable=phone_variable)
                            if responses and len(responses) > 0:
                                response = responses[0]
                                response.process_count = value
                                response.response_date = timezone.now()
                                response.save()
                            else:
                                response = Response()
                                response.check_result = check_result
                                response.phone_variable = phone_variable
                                response.process_count = value
                                response.response_date = timezone.now()
                                response.save()
            return delivery_detail_with_message(request, delivery_id,
                                                'Confirm Detail for Order No: {} has been saved.'.format(delivery_id))
        except KeyError:
            pass

    return delivery_detail_with_message(request, delivery_id,
                                        message='Failed to save Confirm Detail for Order No: {}.'.format(delivery_id),
                                        feedback_form=form)
