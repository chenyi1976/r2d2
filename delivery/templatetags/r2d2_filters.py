from django import template
from delivery.models import PhoneType, Delivery, DeliveryItem, CheckResult, PhoneVariable, PhoneStatus

register = template.Library()


@register.filter
def getitem(item, string):
    return item.get(string, '')


@register.filter
def get_range(value):
    return range(value)


@register.filter
def check_table(check_form):
    result = '<tr><th>Phone Type</th><th>Received</th><th>Total Qualified</th><th>Pink Mark</th>' \
             '<th>Non OEM</th><th>Fixable TP</th><th>Bad</th></tr>'
    current_type_id = ''
    for field_name in check_form.fields:
        field = check_form.fields[field_name]
        if field_name.startswith('check_'):
            type_id, status_id = field_name[len('check_'):].split('_')
            if type_id != current_type_id:
                result += '<tr>'
                current_type_id = type_id
                phone_type = PhoneType.objects.get(pk=type_id)

                try:
                    delivery_id = check_form.initial['delivery_id']
                except KeyError:
                    delivery_id = check_form.cleaned_data['delivery_id']

                delivery = Delivery.objects.get(pk=delivery_id)
                delivery_items = DeliveryItem.objects.filter(delivery=delivery, phone_type=type_id)
                item_count = '0'
                if delivery_items:
                    item_count = str(delivery_items[0].received)

                result += '<td>' + phone_type.name + '</td><td>' + item_count + '</td>'
            result += '<td><input class="input-sm" style="width:60px" id="id_' + field_name + '" name="' + field_name \
                      + '" type="number" min="0" value="' + str(field.initial) + '"></td>'

    return result


@register.filter
def check_table_print(check_form):
    result = '<tr><th>Phone Type</th><th>Received</th><th>Total Qualified</th><th>Pink Mark</th>' \
             '<th>Non OEM</th><th>Fixable TP</th><th>Bad</th></tr>'
    current_type_id = ''
    for field_name in check_form.fields:
        field = check_form.fields[field_name]
        if field_name.startswith('check_'):
            type_id, status_id = field_name[len('check_'):].split('_')
            if type_id != current_type_id:
                result += '<tr>'
                current_type_id = type_id
                phone_type = PhoneType.objects.get(pk=type_id)

                try:
                    delivery_id = check_form.initial['delivery_id']
                except KeyError:
                    delivery_id = check_form.cleaned_data['delivery_id']

                delivery = Delivery.objects.get(pk=delivery_id)
                delivery_items = DeliveryItem.objects.filter(delivery=delivery, phone_type=type_id)
                item_count = '0'
                if delivery_items:
                    item_count = str(delivery_items[0].received)

                result += '<td>' + phone_type.name + '</td><td>' + item_count + '</td>'
            result += '<td>' + str(field.initial) + '</td>'

    return result


@register.filter
def feedback_table(feedback_form):
    result = '<tr><th>Phone Type</th><th>Received</th><th>Type</th><th>Total Qualified</th><th>Pink Mark</th>' \
             '<th>Non OEM</th><th>Fixable TP</th><th>Bad</th></tr>'
    try:
        delivery_id = feedback_form.initial['delivery_id']
    except KeyError:
        delivery_id = feedback_form.cleaned_data['delivery_id']
    delivery = Delivery.objects.get(pk=delivery_id)

    phone_types = PhoneType.objects.all()
    phone_statuses = PhoneStatus.objects.all()

    for phone_type in phone_types:
        phone_variables = PhoneVariable.objects.filter(phone_type=phone_type)

        delivery_items = DeliveryItem.objects.filter(delivery=delivery, phone_type=phone_type)
        item_count = '0'
        if delivery_items:
            item_count = str(delivery_items[0].received)

        row_span = str(len(phone_variables) + 2)
        # column 0, display phone type name with count.
        result += '<tr class="success">'
        result += '<td style="vertical-align: middle" rowspan="' + row_span + '">' + phone_type.name \
                  + '</td><td style="vertical-align: middle" rowspan="' + row_span + '">' + item_count + '</td>'

        # 1, Test Result Row

        result += '<td>'
        result += '<div>Test Result</div>'
        result += '</td>'

        for phone_status in phone_statuses:
            # test result count
            delivery_items = DeliveryItem.objects.filter(delivery=delivery, phone_type=phone_type)
            check_result_count = '0'
            if delivery_items:
                check_results = CheckResult.objects.filter(delivery_item=delivery_items[0],
                                                           phone_status=phone_status)
                if check_results:
                    for check_result in check_results:
                        check_result_count = str(check_result.count)

            result += '<td><div id="id_fc_' + str(phone_type.id) + "_" + str(phone_status.id) + '">' \
                      + check_result_count + '</div></td>'

        result += '</tr>'

        # 2, Phone Variable Row (Refurblish)

        for phone_variable in phone_variables:
            result += '<tr class="info">'
            result += '<td><div>' + phone_variable.name + '</div></td>'

            # columns for status
            for phone_status in phone_statuses:
                field_name = "feedback_" + str(phone_type.id) + "_" + str(phone_status.id) + "_" + str(
                    phone_variable.id)
                field_value = feedback_form.fields[field_name].initial
                result += '<td><div id="div_' + field_name + '">' \
                          + '<input class="input-sm" style="width:70px;" id="id_' \
                          + field_name + '" name="' + field_name + '" type="number" min="0" value="' + str(field_value) \
                          + '"></div></td>'

            result += '</tr>'

        # 3, Buyback row Row
        result += '<tr class="danger">'

        result += '<td><div>Buyback</div>'
        result += '</td>'

        for phone_status in phone_statuses:
            result += '<td id="id_buyback_' + str(phone_type.id) + "_" + str(phone_status.id) + '">0</td>'

        result += '</tr>'

    return result


@register.filter
def feedback_table_print(feedback_form):
    result = '<tr><th>Phone Type</th><th>Received</th><th>Type</th><th>Total Qualified</th><th>Pink Mark</th>' \
             '<th>Non OEM</th><th>Fixable TP</th><th>Bad</th></tr>'
    try:
        delivery_id = feedback_form.initial['delivery_id']
    except KeyError:
        delivery_id = feedback_form.cleaned_data['delivery_id']
    delivery = Delivery.objects.get(pk=delivery_id)

    phone_types = PhoneType.objects.all()
    phone_statuses = PhoneStatus.objects.all()

    for phone_type in phone_types:
        phone_variables = PhoneVariable.objects.filter(phone_type=phone_type)

        delivery_items = DeliveryItem.objects.filter(delivery=delivery, phone_type=phone_type)
        item_count = '0'
        if delivery_items:
            item_count = str(delivery_items[0].received)

        row_span = str(len(phone_variables) + 2)
        # column 0, display phone type name with count.
        result += '<tr class="success">'
        result += '<td style="vertical-align: middle" rowspan="' + row_span + '">' + phone_type.name \
                  + '</td><td style="vertical-align: middle" rowspan="' + row_span + '">' + item_count + '</td>'

        # 1, Test Result Row

        result += '<td>'
        result += '<div>Test Result</div>'
        result += '</td>'

        for phone_status in phone_statuses:
            # test result count
            delivery_items = DeliveryItem.objects.filter(delivery=delivery, phone_type=phone_type)
            check_result_count = '0'
            if delivery_items:
                check_results = CheckResult.objects.filter(delivery_item=delivery_items[0],
                                                           phone_status=phone_status)
                if check_results:
                    for check_result in check_results:
                        check_result_count = str(check_result.count)

            result += '<td><div>' + check_result_count + '</div></td>'

        result += '</tr>'

        # 2, Phone Variable Row (Refurblish)

        for phone_variable in phone_variables:
            result += '<tr class="info">'
            result += '<td><div>' + phone_variable.name + '</div></td>'

            # columns for status
            for phone_status in phone_statuses:
                field_name = "feedback_" + str(phone_type.id) + "_" + str(phone_status.id) + "_" + str(
                    phone_variable.id)
                field_value = feedback_form.fields[field_name].initial
                result += '<td>' + str(field_value) + '</td>'

            result += '</tr>'

        # 3, Buyback row Row
        result += '<tr class="danger">'

        result += '<td><div>Buyback</div>'
        result += '</td>'

        for phone_status in phone_statuses:
            result += '<td id="id_buyback_' + str(phone_type.id) + "_" + str(phone_status.id) + '">0</td>'

        result += '</tr>'

    return result
