from django.contrib.auth.models import User
from django.test import TestCase
import re


class DeliveryTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user(username='c1', email='c1@example.com', password='c1')
        admin_user = User.objects.create_user(username='admin', email='admin@example.com', password='admin')
        admin_user.is_superuser = True
        admin_user.save()
        # print 'admin is super user:' + str(admin_user.is_superuser)

    def test_any_url_not_login(self):
        response = self.client.get("/xyz")
        self.assertEqual(response.status_code, 302)

    # def test_phone_login(self):
    #     login = self.client.login(username='c1', password='c1')
    #     self.assertTrue(login)
    #
    #     response = self.client.get("/phone")
    #     # print 'content:' + response.content
    #     # print response.context.keys()
    #     # print response.context['view']
    #     # print response.context['user']
    #     t = re.findall('return_code:(\d*)', response.content)
    #     self.assertTrue(len(t) == 1)
    #     # print "return code:%d".format(t[0])
    #     self.assertEqual(t[0], '0')
    #     self.assertEqual(response.status_code, 200)

    def test_all_url_with_login(self):
        urls = ["/phone", "/phone/add", "/phone/detail/0", "/price", "/payment"]

        login = self.client.login(username='c1', password='c1')
        self.assertTrue(login)

        for url in urls:
            print 'url:' + url
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)

